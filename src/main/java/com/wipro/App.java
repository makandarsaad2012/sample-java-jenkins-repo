package com.wipro;

import com.wipro.model.Greeting;

public class App {
	public static void main(String[] args) {
		System.out.println(new Greeting("Hello World!").getMessage());
	}
}
